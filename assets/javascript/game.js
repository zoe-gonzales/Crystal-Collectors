
// document.ready function
$(document).ready(function () {
// Global Variables
// variable that holds numToGuess
var targetNum;
// variable that holds user's current score
var userScore;
// variable (array) that holds the value of each crystal
var crystalImages = [
    $("<img>").attr("src", "./assets/images/crystal-one.png"),
    $("<img>").attr("src", "./assets/images/crystal-two.png"),
    $("<img>").attr("src", "./assets/images/crystal-three.png"),
    $("<img>").attr("src", "./assets/images/crystal-four.png")
];

// Counter variables for wins and losses
var winCounter = 0;
var lossCounter = 0;
var crystalValues = [1, 3, 6, 10];

// Function Declarations
function startGame() {
    // To reset game..
    userScore = 0;
    // Selects # from 19 to 120 and saves to targetNum var
    targetNum = Math.floor(Math.random() * 100) + 19;
    // displays the target number
    $("#numToGuess").text(targetNum);
    $("#userScore").text(userScore); 

    // Function shuffles values in array
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    // Calling shuffle function for crystal images and values
    shuffle(crystalImages);
    shuffle(crystalValues);

    // Assigns each crystal a data attribute from the crystalValues array
    crystalImages.forEach((image,value) => {
        image.addClass("crystal");
        image.attr("data-crystalvalue", crystalValues[value]);
    });
    
    // Appends crystal images to page
    $("#crystal-container").append(crystalImages);
}

function playGame() {
    startGame()
    // On Click function
    $(".crystal").on("click", function () {
        var crystalValue = parseInt($(this).attr("data-crystalvalue"));
        userScore += crystalValue;
        $("#userScore").text(userScore);
        // Conditional statement that determines if the user's score equals the numToGuess or not
        if (userScore === targetNum) {
            // If it equals, winCounter increments by 1, shows winning messages
            alert("You won!");
            winCounter++;
            $("#Wins").text(winCounter);
            startGame();
        // If it does not equal, lossCounter increments by 1 and shows losing message
        } else if (userScore >= targetNum) {
            alert("You lost!");
            lossCounter++;
            $("#Losses").text(lossCounter);
            startGame();
        }
    });
}
// Run game
playGame();
});