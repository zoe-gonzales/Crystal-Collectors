# unit-4-game

## Crystal Collectors Game

## To play the game:
* Click on any one of the crystals to discover its value. The amount will not be revealed until the crystal is clicked. 
* Try to reach the target number exactly. If the target number is surpassed, the game is lost and automatically reset.
* With each round, the crystal location and value is reshuffled.
* Wins and losses are tallied.

### Tech used: Javascript, jQuery, Bootstrap, CSS

### Start Page

![Start Page](assets/images/start-page.png)

### Playing the Game

![Game Played](assets/images/game-played.png)

[Deployed link](https://zoe-gonzales.github.io/unit-4-game/)
